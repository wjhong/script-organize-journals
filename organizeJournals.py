
# Used to organize my journal entries on Google Drive
import re
import pickle
import os.path
from datetime import datetime 
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

SCOPES = ['https://www.googleapis.com/auth/documents','https://www.googleapis.com/auth/drive']
DISCOVERY_DOC = 'https://docs.googleapis.com/$discovery/rest?version=v1'

# Currently testdata
TARGET_DIRECTORY_ID = '188MWlu3R41hCQJfvZX0WvHCkoiZEul-q'


def get_credentials():
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth 2.0 flow is completed to obtain the new credentials.

    The file token.pickle stores the user's access and refresh tokens, and is
    created automatically when the authorization flow completes for the first
    time.

        Returns:
            Credentials, the obtained credential.
    """
    creds = None
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    return creds


def read_paragraph_element(element):
    """Returns the text in the given ParagraphElement.

        Args:
            element: a ParagraphElement from a Google Doc.
    """
    text_run = element.get('textRun')
    if not text_run:
        return ''
    return text_run.get('content')


def read_strucutural_elements(elements):
    """Recurses through a list of Structural Elements to read a document's text where text may be
        in nested elements.

        Args:
            elements: a list of Structural Elements.
    """
    text = ''
    for value in elements:
        if 'paragraph' in value:
            elements = value.get('paragraph').get('elements')
            for elem in elements:
                text += read_paragraph_element(elem)
        elif 'table' in value:
            # The text in table cells are in nested Structural Elements and tables may be
            # nested.
            table = value.get('table')
            for row in table.get('tableRows'):
                cells = row.get('tableCells')
                for cell in cells:
                    text += read_strucutural_elements(cell.get('content'))
        elif 'tableOfContents' in value:
            # The text in the TOC is also in a Structural Element.
            toc = value.get('tableOfContents')
            text += read_strucutural_elements(toc.get('content'))
    return text


def get_document_content(document_id):
    """Retrieves all text inside of a document

        Args: DocumentId, ID of the document text should be retrieved from.
    """
    doc = docs_service.documents().get(documentId=document_id).execute()
    doc_content = doc.get('body').get('content')
    return read_strucutural_elements(doc_content)


def parse_entries(data):
    """Parses text blobs into entries and returns a list of entries

        Returns:
            Entries, list of entries parsed from provided data
    """
    lines = data.splitlines()
    idx = 0
    name = ""
    date=""
    entry = []
    entries = []

    # Breaks up entries by checking for a date pattern at the head of each entry.
    for line in lines:
        pattern = re.match("\\d\\d?\\.\\d\\d?\\.\\d{4}", line)
        if (pattern):
            if entry:
                entry_obj = {'date': date, 'name': name, 'content': "\n\n".join(entry)}
                entries.append(entry_obj)
            idx = 0
            name = line.strip()
            date = pattern.group()
            entry = []
        # First three lines in an entry are the header, which should not be spaced.
        # All other newlines are paragraph breaks, so add an extra newline for clarity.
        line = line if idx < 2 else line + "\n"
        entry.append(line)
        idx += 1

    if entry:
        entry_obj = {'date': date, 'name': name, 'content': "\n\n".join(entry)}
        entries.append(entry_obj)

    return entries


def get_all_docs(directory_id):
    """Recursively gets ids of documents inside a given directory.

    Query is the list of things that have the parent that is the directory_id.

    Fields tells the service to return the fileID fileName and fileMimetype as keys.

        Args:
            Directory, id of a directory to search for documents
        Return:
            Entries, an object list of all formatted entries in the provided directory
            Volume, an object list of all volumes in the provided directory
    """
    query = "'{}' in parents".format(directory_id)
    directory_children = drive_service.files().list(q=query, fields="files(id, name, mimeType)").execute()

    entries = []
    volumes = []

    """If it's a directory, recurse into it.
        Else, push it into the corresponding list.
        If it doesn't fit any of them, stop script.
    """
    for file in directory_children.get('files', []):
        if (file.get("mimeType") == "application/vnd.google-apps.folder"):
            for list, doc in zip([entries, volumes], get_all_docs(file.get("id"))):
                list.extend(doc)
        elif ("Volume" in file.get("name")):
            volumes.append(file)
        elif (pattern:=re.match("\\d\\d?\\.\\d\\d?\\.\\d{4}", file.get("name"))):
            file['date'] = pattern.group()
            entries.append(file)
        else:
            raise Exception("Unsupported File: {}".format(file.get("name")))

    return entries, volumes

def duplicate_existing_docs(directory, entries):
    """Duplicates every document in provided list of entries

        Args:
            Directory, the initial storage location of documents
            Entries, a list of document entries to duplicate
        Return:
            DuplicatedDocs, a list of {ids, date, name} of duplicated docs
    """
    duplicated_docs = []

    for entry in entries:
        # Set file metadata
        file_metadata = {
            'name': entry.get('name'),
            'parents': [directory]
        }

        # Copy entry doc
        drive_response = drive_service.files().copy(fileId=entry.get('id'), body=file_metadata).execute()
        
        # Create new dupe obj and append to duplicated docs list
        duped_doc = {
            'id': drive_response.get('id'),
            'name': drive_response.get('name'),
            'date': entry.get('date')
        }

        duplicated_docs.append(duped_doc)

    return duplicated_docs

def create_entry_docs(directory, entries):
    """Creates a document for each entry in provided list of entries

        Args:
            Directory, the initial storage location of documents
            Entries, a list of entries to make a document for
        Return:
            Entries, same list as entries with the newly created ID appended
    """

    for entry in entries:
        # Set file metadata
        file_metadata = {
            'name': entry.get('name'),
            'mimeType': 'application/vnd.google-apps.document',
            'parents': [directory]
        }

        # Created doc
        file = drive_service.files().create(body=file_metadata,
                                        fields='id').execute()

        # Add newly created doc ID to the entry obj
        entry['id'] = file.get('id')

        # Create request body
        requests = {
            'insertText': {
                'location': {
                    'index': 1,
                },
                'text': entry.get('content')
            }
        }

        # Add content to document that was just created
        docs_service.documents().batchUpdate(documentId=file.get('id'),
                                                body={'requests': requests}).execute()

    # Return entries with the new IDs appended
    return entries


def sort_docs(directory, entries):
    """Sorts documents into years, then creates folders and moves the appropriate documents into them.

        Args:
            Entries, the list of documents to sort.
        Return:
            The list of directories that were created with the documents inside.
    """
    # Declare list of objs {year : directory_id}
    year_directories = {}

    for entry in entries:
        if (pattern:=re.match("\\d\\d?\\.\\d\\d?\\.(\\d{4})", entry.get('date'))):
            year = pattern.group(1)
            # If year directory doesn't exist, create it
            if not (year in year_directories.keys()):
                # Create a new directory with the year as the name inside of the generated directory.
                new_metadata = {
                    'name': year,
                    'mimeType': 'application/vnd.google-apps.folder',
                    'parents': [directory]
                }

                new_year_directory = drive_service.files().create(body=new_metadata,
                                                                    fields='id').execute()

                # Add the new key value pair to the list
                year_directories[year] = new_year_directory.get('id')

            # Move entry into the year directory that it belongs in
            file = drive_service.files().get(fileId=entry.get('id'),
                                                fields='parents').execute()

            # Get previous parents so we can remove them.
            previous_parents = ",".join(file.get('parents'))
            new_parent = year_directories[year]

            # Move the file to the new folder
            file = drive_service.files().update(fileId=entry.get('id'),
                                                addParents=new_parent,
                                                removeParents=previous_parents,
                                                fields='id, parents').execute()
        else:
            raise Exception("Invalid date: {}".format(entry.get("date")))

    # Return list of all the newly created directory names
    return list(year_directories.keys())

def main():
    """Takes provided drive directory ID and turns every single journal entry into a new document.

    1. Should work recursively from the root, and ignore all duplicates.
    2. Creates a new directory at drive root to store all entries, called "Journals [GENERATED]".
        2a. If "Journals [GENERATED]" already exists, stop script with warning.
    3. The new directory should also contain directories by year, with entries sorted into them.
    4. Entries titled with their date.
    5. First three lines of each document (the header) should be formatted as usual.

    testdata - Total Entry Count: 7 + 3 + 2 + 3 = 15

    Notes: So since standalones already have ids they don't even contain any of the text in it.
        However, volume entries still need to be parsed, so they need to actually be turned into docs first.
        Then after they're converted into docs, we can ignore the content and combine the two, sort by date.
        After date sorting, we make a directory that we move all these new docs into.

    standalone obj: {id, date, name, mimeType}  // content doesn't matter for the standalones since it's already in docs
    before docs parsed obj: {date, name, content}
    after docs parsed obj: {id, data, name, content} 

    Right now we need to duplicate standalones and also create new docs for parsed entries.
    To get them into the same parent directory, we should do that after we sort.
    1. Dupe standalone entries
    2. Create docs for parsed entries
    3. Merge the two lists
    4. Sort the combined list by date
    5. Create Root directory (Journals [GENERATED])
    6. Create folder for first item's year, make that the child of Root
    7. While item year is in that directory year, make folder item's parent
    8. If the year no longer matches, create new folder, etc until done.
    """
    # Make services globally available for ease
    global docs_service, drive_service
    creds = get_credentials()
    docs_service = build('docs', 'v1', credentials=creds)
    drive_service = build('drive', 'v3', credentials=creds)

    # Get ids of already formatted entries, and ids of volumes
    entries, volumes = get_all_docs(TARGET_DIRECTORY_ID)

    # Standlone entries
    print("\nStandalone Formatted Entries ({})".format(len(entries)))
    for entry in entries:
        print("- ", entry.get("name"))

    # Volumes
    print("\nVolumes:")
    parsed_entries = []
    for volume in volumes:
        # Get data from the id and parse entries
        data = get_document_content(volume.get("id"))
        parsed_volume = parse_entries(data)

        # Print
        print("\n- {} ({})".format(volume.get("name"), len(parsed_volume)))
        for e in parsed_volume:
            print("   - ", e.get("name"))
        
        # Add the parsed volume entries into the greater parsed entries list
        parsed_entries.extend(parsed_volume)

    # Create initial directory to store all documents
    generated_metadata = {
        'name': '[GENERATED] Journals',
        'mimeType': 'application/vnd.google-apps.folder',
    }

    generated_directory = drive_service.files().create(body=generated_metadata,
                                        fields='id').execute()

    print ("\nCreated initial directory with ID: %s" % generated_directory.get('id'))

    # Duplicate document for each standalone entry
    print("\nDuplicating standalone entries...")
    docs_from_standalone = duplicate_existing_docs(generated_directory.get('id'), entries)
    print("Duplicated {} documents from standalone entries.".format(len(docs_from_standalone)))

    # Create document for each parsed entries in volumes
    print("\nCreating entries from volumes...")
    docs_from_volumes = create_entry_docs(generated_directory.get('id'), parsed_entries)
    print("Created {} documents from volumes.".format(len(docs_from_volumes)))

    # Sort docs into directories by year
    print("\nSorting combined entries into directories by year...")
    combined_entries = docs_from_standalone + docs_from_volumes
    sorted_doc_directories = sort_docs(generated_directory.get('id'), combined_entries)
    print("Created directories:")
    for d in sorted_doc_directories:
        print("   - ", d)

    print("\nVolume Count: ", len(volumes))
    print("Total Entry Count: ", len(docs_from_standalone) + len(docs_from_volumes))

    # Should try to figure out tabs, bolds, and italics inside of the creation function
    # Debug newline problem in the final doc created from volumes

if __name__ == "__main__":
    main()
    print('Done!')
